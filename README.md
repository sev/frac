Trail Marker
------------

This repository provides Trail Marker, an experimental
OpenType frac feature implementation that offers the
same functionality as Fraction Fever 2 by Tal Leming,
but has a few potential advantages.

For one, it can process fractions of arbitrary length.
As a result, incomplete or incorrect replacements are
avoided more reliably. At the same time, it adds only
about a third as many bytes to a font. When compiled
with FontTools, this is the approximate overhead:

|                    |   ttf |  woff | woff2 |
| ------------------ | ----: | ----: | ----: |
| Fraction Fever     |  1150 |   440 |   420 |
| Trail Marker       |   400 |   180 |   160 |
| Timid Trail Marker |   510 |   220 |   210 |

To my knowledge, the features adhere to the OpenType
specification. However, not all renderers provide full
OpenType support, and extensive testing is necessary.

I share the Trail Marker implementation in the hope that
it might be useful, or at least interesting, to others.


License
-------

This font software is made available under the terms of the
[Creative Commons CC0 1.0][CC0] Universal Public Domain Dedication.

[CC0]: https://creativecommons.org/publicdomain/zero/1.0/
