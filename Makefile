# Files

ttfFonts := \
	test/base/FractionFeverBase.ttf \
	test/base/TimidTrailMarkerBase.ttf \
	test/base/TrailMarkerBase.ttf \
	test/FractionFeverTest.ttf \
	test/TimidTrailMarkerTest.ttf \
	test/TrailMarkerTest.ttf


woffFonts := \
	test/base/FractionFeverBase.woff \
	test/base/FractionFeverBase.woff2 \
	test/base/TimidTrailMarkerBase.woff \
	test/base/TimidTrailMarkerBase.woff2 \
	test/base/TrailMarkerBase.woff \
	test/base/TrailMarkerBase.woff2 \
	test/FractionFeverTest.woff \
	test/FractionFeverTest.woff2 \
	test/TimidTrailMarkerTest.woff \
	test/TimidTrailMarkerTest.woff2 \
	test/TrailMarkerTest.woff \
	test/TrailMarkerTest.woff2


# Main rules

.PHONY: ttf
ttf: ${ttfFonts}

.PHONY: woff
woff: ${woffFonts}

.PHONY: calc-overhead
calc-overhead: ttf woff
		@ echo "ttf"
		@ ./tools/size-diff.py test/FractionFeverTest.ttf test/base/FractionFeverBase.ttf
		@ ./tools/size-diff.py test/TrailMarkerTest.ttf test/base/TrailMarkerBase.ttf
		@ ./tools/size-diff.py test/TimidTrailMarkerTest.ttf test/base/TimidTrailMarkerBase.ttf
		@ echo
		@ echo "woff"
		@ ./tools/size-diff.py test/FractionFeverTest.woff test/base/FractionFeverBase.woff
		@ ./tools/size-diff.py test/TrailMarkerTest.woff test/base/TrailMarkerBase.woff
		@ ./tools/size-diff.py test/TimidTrailMarkerTest.woff test/base/TimidTrailMarkerBase.woff
		@ echo
		@ echo "woff2"
		@ ./tools/size-diff.py test/FractionFeverTest.woff2 test/base/FractionFeverBase.woff2
		@ ./tools/size-diff.py test/TrailMarkerTest.woff2 test/base/TrailMarkerBase.woff2
		@ ./tools/size-diff.py test/TimidTrailMarkerTest.woff2 test/base/TimidTrailMarkerBase.woff2

.PHONY: clean
clean:
	@ rm -f ${woffFonts}


# File rules

test/%Test.woff: test/%Test.ttf
	@ ./tools/make-woff.py $^ $@

test/%Test.woff2: test/%Test.ttf
	@ ./tools/make-woff.py $^ $@

test/%Test.ttf: test/base/%Base.ttf feature/%.fea
	@ ./tools/merge-fea.py $^ $@

test/base/%Base.woff: test/base/%Base.ttf
	@ ./tools/make-woff.py $^ $@

test/base/%Base.woff2: test/base/%Base.ttf
	@ ./tools/make-woff.py $^ $@

test/base/%Base.ttf: test/base/BaseFont.sfd
	@ ./tools/make-ttf.py $^ $*Test $@
