#!/usr/bin/env python
# TASK
#   Generate ttf from sfd

import re
import sys
import time
import fontforge

# Read arguments
SOURCE   = sys.argv[1] # *.sfd
FONTNAME = sys.argv[2] # *
TARGET   = sys.argv[3] # *.ttf

# Load font
font = fontforge.open(SOURCE)

# Insert names
font.fontname = FONTNAME
font.fullname = FONTNAME
font.familyname = FONTNAME
font.appendSFNTName("English (US)", "UniqueID",
	font.fullname + " " +
	font.version + " " +
	time.strftime("%y%m%d"))

# Save font
font.generate(TARGET, flags=("opentype", "dummy-dsig"))
font.close()
