#!/usr/bin/env python
# TASK
#   Print size difference of two files

from __future__ import print_function
import os
import sys

# Read arguments
FILE1 = sys.argv[1]
FILE2 = sys.argv[2]

# Print diff
print(str(os.path.getsize(FILE1)), FILE1)
print(str(os.path.getsize(FILE2)), FILE2)
print(str(os.path.getsize(FILE1) - os.path.getsize(FILE2)))
