#!/usr/bin/env python
# TASK
#   Generate woff or woff2

import sys
from fontTools.ttLib import TTFont
from fontTools.ttLib import sfnt

# Read arguments
SOURCE = sys.argv[1] # *.ttf
TARGET = sys.argv[2] # *.woff|*.woff2

# Load font
font = TTFont(SOURCE)
font.flavor = TARGET.split(".")[-1]

# Save woff
sfnt.USE_ZOPFLI = True
sfnt.ZLIB_COMPRESSION_LEVEL = 9
font.save(TARGET)
