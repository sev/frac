#!/usr/bin/env python
# TASK
#   Merge fea into ttf

import sys
from fontTools.ttLib import TTFont
from fontTools.feaLib import builder as feaBuilder

# Read arguments
SOURCE   = sys.argv[1] # *.ttf
FEATURES = sys.argv[2] # *.fea
TARGET   = sys.argv[3] # *.ttf

# Load font
font = TTFont(SOURCE)

# Merge feature file
feaBuilder.addOpenTypeFeatures(font, FEATURES)

# Save font
font.save(TARGET)
